//
//  ViewController.swift
//  ferrettown
//
//  Created by Piland, Herbert on 6/29/15.
//  Copyright (c) 2015 Piland, Herbie. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    @IBOutlet weak var ferretNameTextField: UITextField!
    @IBOutlet weak var ferretNameLabel: UILabel!
    @IBOutlet weak var ferretImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Handle the text field's user input through delegate callbacks.
        ferretNameTextField.delegate = self
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        setLabelToText()
    }
    
    // MARK: Actions
    @IBAction func submitButtonAction(sender: UIButton) {
       setLabelToText()
    }
    
    
    @IBAction func ferretImageAction(sender: UITapGestureRecognizer) {
        ferretNameLabel.text = "Hey, watch it!"
    }
    
    // MARK: Functionality
    func setLabelToText(){
        if ferretNameTextField.text.rangeOfString("Nice name") != nil{
            
            ferretNameLabel.text = ferretNameTextField.text + "? Cool!"
        }
        else{
            
            ferretNameLabel.text = ferretNameTextField.text + "? Nice name!"
        }
        
        ferretNameTextField.text = ""
    }
    
}

